<?php

use App\Models\Plan;
use App\Models\Tenant;
use Illuminate\Database\Seeder;

class TenantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plan = Plan::first();

        $plan->tenants()->create([
            'cnpj' => '1234567894',
            'name' => 'MarhaSoft',
            'url' => 'marhasoft',
            'email' => 'samuelfesant@gmail.com',
        ]);
    }
}
