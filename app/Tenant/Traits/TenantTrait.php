<?php

namespace App\Tenant\Traits;

use App\Tenant\Observers\TenantObserver;
use App\Tenant\Scopes\TenantScope;

trait TenantTrait
{
    /**
     * Reescrevendo metodo boot
     * Responsavel por adicionar globalmente o id do tenant autenticado automaticamente
     * Ira retorna apenas os dados do tenant id autenticado no momento
     */
    protected static function boot()
    {
        parent::boot();

        static::observe(TenantObserver::class);

        static::addGlobalScope(new TenantScope);
    }
}
