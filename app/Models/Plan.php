<?php

namespace App\Models;

use App\Models\Traits\FilterTrait;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use FilterTrait;

    protected $fillable = ['name', 'description', 'price', 'url'];

    public function details()
    {
        return $this->hasMany(DetailPlan::class);
    }

    public function profiles()
    {
        return $this->belongsToMany(Profile::class);
    }

    public function tenants()
    {
        return $this->hasMany(Tenant::class);
    }

    /**
     * Profiles not linked with this plan
     */
    public function profilesAvailable($filter = null)
    {
        $profiles = Profile::whereNotIn('profiles.id', function ($query) {
            $query->select('plan_profile.profile_id')
                ->from('plan_profile')
                ->whereRaw("plan_profile.plan_id={$this->id}");
        })
            ->where(function ($queryFilter) use ($filter) {
                if ($filter) {
                    $queryFilter->where('profiles.name', 'LIKE', "%{$filter}%");
                }
            })->paginate();

        return $profiles;
    }
}
