<?php

namespace App\Models;

use App\Models\Traits\FilterTrait;
use App\Tenant\Traits\TenantTrait;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use TenantTrait, FilterTrait;

    protected $fillable = ['name', 'url', 'price', 'description', 'image'];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
    * Categories not linked with this product
    */
    public function categoriesAvailable($filter = null)
    {
        $categories = Category::whereNotIn('categories.id', function ($query) {
            $query->select('category_product.category_id')
                ->from('category_product')
                ->whereRaw("category_product.product_id={$this->id}");
        })
            ->where(function ($queryFilter) use ($filter) {
                if ($filter) {
                    $queryFilter->where('categories.name', 'LIKE', "%{$filter}%");
                }
            })->paginate();

        return $categories;
    }
}
