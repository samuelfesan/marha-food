<?php

namespace App\Models\Traits;

trait UserACLTrait
{
    /**
     * Retorna as permissoes do usuario autenticado
     */
    public function permissions()
    {
        $tenant = $this->tenant()->first();
        $plan = $tenant->plan; //Recupera o plano do tenant

        $permissions = [];
        //Recupera os perfis dos planos(Cada plano tem seu perfil)
        foreach ($plan->profiles as $profile) {
            //Recupera as permissoes dos perfis
            foreach ($profile->permissions as $permission) {
                array_push($permissions, $permission->slug);
            }
        }

        return $permissions;
    }

    /**
     * Verifica se o usuario autenticado tem a permissao
     */
    public function hasPermission(string $permissionName): bool
    {
        return in_array($permissionName, $this->permissions());
    }

    public function isAdmin()
    {
        return in_array($this->email, config('tenant.admins'));
    }
}
