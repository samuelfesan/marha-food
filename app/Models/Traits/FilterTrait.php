<?php

namespace App\Models\Traits;

trait FilterTrait
{
    public function search($filter = null)
    {
        return $this->where('name', 'LIKE', "%{$filter}%")
            ->orWhere('description', 'LIKE', "%{$filter}%")
            ->paginate();
    }
}
