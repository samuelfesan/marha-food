<?php

namespace App\Models;

use App\Models\Traits\FilterTrait;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use FilterTrait;

    protected $fillable = ['name', 'description'];

    public function profiles()
    {
        return $this->belongsToMany(Profile::class);
    }

}
