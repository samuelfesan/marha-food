<?php

namespace App\Traits;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

trait MessageTrait
{
    private function messageStatus($status, $message = null)
    {
        $messageToast = config("message.{$status}.message");

        if ($status == 'info' && isset($message)) {
            $messageToast = $message;
        }

        return  toastr()->$status($messageToast);
    }
}
