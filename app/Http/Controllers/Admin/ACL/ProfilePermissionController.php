<?php

namespace App\Http\Controllers\Admin\ACL;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Profile;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class ProfilePermissionController extends Controller
{
    use MessageTrait;

    protected  $profile, $permission;

    public function __construct(Profile $profile, Permission $permission)
    {
        $this->profile = $profile;
        $this->permission = $permission;
    }

    public function permissions($idProfile)
    {
        $this->authorize('visualizar-permissoes');
        if (!$profile = $this->profile->find($idProfile)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $permissions = $profile->permissions()->paginate();

        return view('admin.pages.profiles.permissions.permissions',
            compact('profile', 'permissions'));
    }

    public function profiles($idPermission)
    {
        $this->authorize('visualizar-permissoes');
        if (!$permission = $this->permission->find($idPermission)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $profiles = $permission->profiles()->paginate();

        return view('admin.pages.permissions.profiles.profiles',
            compact('permission', 'profiles'));
    }

    public function permissionsAvailable(Request $request, $idProfile)
    {
        $this->authorize('visualizar-permissoes');
        if (!$profile = $this->profile->find($idProfile)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $filters = $request->except('_token');

        $permissions = $profile->permissionsAvailable($request->filter);

        return view('admin.pages.profiles.permissions.available',
            compact('profile', 'permissions', 'filters'));
    }

    public function attachPermissionsProfile(Request $request, $idProfile)
    {
        $this->authorize('cadastrar-permissoes');
        if (!$profile = $this->profile->find($idProfile)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        if (!$request->permissions || count($request->permissions) == 0) {
            $this->messageStatus('info', 'Desculpe, você precisa escolher pelo menos uma permissão');
            return redirect()->route('profiles.permissions', $profile->id);
        }

        try {

            $profile->permissions()->attach($request->permissions);

            $this->messageStatus('success');

            return redirect()->route('profiles.permissions', $profile->id);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('profiles.permissions', $profile->id);
        }
    }

    public function detachPermissionsProfile($idProfile, $idPermission)
    {
        $this->authorize('deletar-permissoes');
        $profile = $this->profile->find($idProfile);
        $permission = $this->permission->find($idPermission);

        if (!$profile || !$permission) {
            $this->messageStatus('error');
            return redirect()->route('profiles.permissions', $profile->id);
        }

        try {

            $profile->permissions()->detach($permission);

            $this->messageStatus('success');

            return redirect()->route('profiles.permissions', $profile->id);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('profiles.permissions', $profile->id);
        }
    }
}
