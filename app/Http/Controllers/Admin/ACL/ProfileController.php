<?php

namespace App\Http\Controllers\Admin\ACL;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateProfile;
use App\Models\Profile;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    use MessageTrait;

    protected $profile;

    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }

    public function index()
    {
        $this->authorize('visualizar-perfis');
        $profiles = $this->profile->paginate();

        return view('admin.pages.profiles.index', compact('profiles'));
    }

    public function create()
    {
        $this->authorize('cadastrar-perfis');
        return view('admin.pages.profiles.create');
    }

    public function store(StoreUpdateProfile $request)
    {
        $this->authorize('cadastrar-perfis');
        try {
            $this->profile->create($request->all());

            return redirect()->route('profiles.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('profiles.index');
        }

    }

    public function show($id)
    {
        $this->authorize('visualizar-perfis');
        $profile = $this->profile->find($id);

        if (!$profile) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        return view('admin.pages.profiles.show', compact('profile'));
    }

    public function edit($id)
    {
        $this->authorize('editar-perfis');
        $profile = $this->profile->find($id);

        if (!$profile) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        return view('admin.pages.profiles.edit', compact('profile'));
    }

    public function update(StoreUpdateProfile $request, $id)
    {
        $this->authorize('editar-perfis');
        $profile = $this->profile->find($id);

        if (!$profile) {
            return redirect()->back($this->messageStatus('warning'));
        }
        try {
            $profile->update($request->all());
            $this->messageStatus('success');

            return redirect()->route('profiles.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('profiles.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-perfis');
        $profile = $this->profile->find($id);

        if (!$profile) {
            return redirect()->back($this->messageStatus('warning'));
        }
        try {
            $profile->delete();

            $this->messageStatus('success');
            return redirect()->route('profiles.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('profiles.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->only('filter');
        $profiles = $this->profile->search($request->filter);

        return view('admin.pages.profiles.index', compact('profiles', 'filters'));
    }
}
