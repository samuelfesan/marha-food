<?php

namespace App\Http\Controllers\Admin\ACL;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Plan;
use App\Models\Profile;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class PlanProfileController extends Controller
{
    use MessageTrait;

    protected  $plan, $profile;

    public function __construct(Plan $plan, Profile $profile)
    {
        $this->plan = $plan;
        $this->profile = $profile;
    }

    public function profiles($idPlan)
    {
        $this->authorize('visualizar-planos');
        if (!$plan = $this->plan->find($idPlan)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $profiles = $plan->profiles()->paginate();

        return view('admin.pages.plans.profiles.profiles',
            compact('plan', 'profiles'));
    }

    public function plans($idPlan)
    {
        $this->authorize('visualizar-planos');
        if (!$profile = $this->profile->find($idPlan)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $plans = $profile->plans()->paginate();

        return view('admin.pages.profiles.plans.plans',
            compact('plans', 'profile'));
    }

    public function profilesAvailable(Request $request, $idPlan)
    {
        $this->authorize('visualizar-planos');
        if (!$plan = $this->plan->find($idPlan)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $filters = $request->except('_token');

        $profiles = $plan->profilesAvailable($request->filter);

        return view('admin.pages.plans.profiles.available',
            compact('plan', 'profiles', 'filters'));
    }

    public function attachProfilesPlan(Request $request, $idPlan)
    {
        $this->authorize('cadastrar-planos');
        if (!$plan = $this->plan->find($idPlan)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        if (!$request->profiles || count($request->profiles) == 0) {
            $this->messageStatus('info', 'Desculpe, você precisa escolher pelo menos uma permissão');
            return redirect()->route('plans.profiles', $plan->id);
        }

        try {

            $plan->profiles()->attach($request->profiles);

            $this->messageStatus('success');

            return redirect()->route('plans.profiles', $plan->id);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('plans.profiles', $plan->id);
        }
    }

    public function detachProfilesPlan($idPlan, $idProfile)
    {
        $this->authorize('deletar-planos');
        $plan = $this->plan->find($idPlan);
        $profile = $this->profile->find($idProfile);

        if (!$plan || !$profile) {
            $this->messageStatus('error');
            return redirect()->route('plans.profiles', $plan->id);
        }

        try {

            $plan->profiles()->detach($profile);

            $this->messageStatus('success');

            return redirect()->route('plans.profiles', $plan->id);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('plans.profiles', $plan->id);
        }
    }
}
