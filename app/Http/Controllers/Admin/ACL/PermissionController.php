<?php

namespace App\Http\Controllers\Admin\ACL;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdatePermission;
use App\Models\Permission;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    use MessageTrait;

    protected $permission;

    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }

    public function index()
    {
        $this->authorize('visualizar-permissoes');
        $permissions = $this->permission->paginate();

        return view('admin.pages.permissions.index', compact('permissions'));
    }

    public function create()
    {
        $this->authorize('cadastrar-permissoes');
        return view('admin.pages.permissions.create');
    }

    public function store(StoreUpdatePermission $request)
    {
        $this->authorize('cadastrar-permissoes');
        try {
            $this->permission->create($request->all());

            return redirect()->route('permissions.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('permissions.index');
        }

    }

    public function show($id)
    {
        $this->authorize('visualizar-permissoes');
        $permission = $this->permission->find($id);

        if (!$permission) {
            return redirect()->back($this->messageStatus('warning'));
        }
        return view('admin.pages.permissions.show', compact('permission'));
    }

    public function edit($id)
    {
        $this->authorize('editar-permissoes');
        $permission = $this->permission->find($id);

        if (!$permission) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        return view('admin.pages.permissions.edit', compact('permission'));
    }

    public function update(StoreUpdatePermission $request, $id)
    {
        $this->authorize('editar-permissoes');
        $permission = $this->permission->find($id);

        if (!$permission) {
            return redirect()->back($this->messageStatus('warning'));
        }
        try {
            $permission->update($request->all());
            $this->messageStatus('success');

            return redirect()->route('permissions.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('permissions.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-permissoes');
        $permission = $this->permission->find($id);

        if (!$permission) {
            return redirect()->back($this->messageStatus('warning'));
        }
        try {
            $permission->delete();

            $this->messageStatus('success');
            return redirect()->route('permissions.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('permissions.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->only('filter');
        $permissions = $this->permission->search($request->filter);

        return view('admin.pages.permissions.index', compact('permissions', 'filters'));
    }
}
