<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateCategory;
use App\Models\Category;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use MessageTrait;

    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $this->authorize('visualizar-categorias');
        $categories = $this->category->latest()->paginate();

        return view('admin.pages.categories.index', compact('categories'));
    }

    public function create()
    {
        $this->authorize('cadastrar-categorias');
        return view('admin.pages.categories.create');
    }

    public function store(StoreUpdateCategory $request)
    {
        $this->authorize('cadastrar-categorias');
        try {

            $this->category->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('categories.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('categories.index');
        }
    }

    public function show($id)
    {
        $this->authorize('visualizar-categorias');
        $category = $this->category->find($id);

        if (!$category) {
            $this->messageStatus('warning');
            return redirect()->route('categories.index');
        }
        return view('admin.pages.categories.show', compact('category'));
    }

    public function edit($id)
    {
        $this->authorize('editar-categorias');
        $category = $this->category->find($id);

        if (!$category) {
            $this->messageStatus('warning');
            return redirect()->route('categories.index');
        }

        return view('admin.pages.categories.edit', compact('category'));
    }

    public function update(StoreUpdateCategory $request, $id)
    {
        $this->authorize('editar-categorias');
        $category = $this->category->find($id);

        if (!$category) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $category->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('categories.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('categories.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-categorias');
        $category = $this->category->find($id);

        if (!$category) {
            $this->messageStatus('warning');
            return redirect()->back();
        }

        try {
            $category->delete();

            $this->messageStatus('success');
            return redirect()->route('categories.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('categories.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $categories = $this->category->search($request->filter);

        return view('admin.pages.categories.index', compact('categories', 'filters'));
    }
}
