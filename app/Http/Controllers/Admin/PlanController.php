<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdatePlan;
use App\Models\Plan;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class PlanController extends Controller
{
    use MessageTrait;

    private $plan;

    public function __construct(Plan $plan)
    {
        $this->plan = $plan;
    }

    public function index()
    {
        $this->authorize('visualizar-planos');
        $plans = $this->plan->latest()->paginate();
        return view('admin.pages.plans.index', compact('plans'));
    }

    public function show($url)
    {
        $this->authorize('visualizar-planos');
        $plan = $this->plan->where('url', $url)->first();

        if (!$plan) {
            $this->messageStatus('warning');
            return redirect()->route('plans.index');
        }
        return view('admin.pages.plans.show', compact('plan'));
    }

    public function create()
    {
        $this->authorize('cadastrar-planos');
        return view('admin.pages.plans.create');
    }

    public function store(StoreUpdatePlan $request)
    {
        $this->authorize('cadastrar-planos');
        try {

            $this->plan->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('plans.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('plans.index');
        }

    }

    public function edit($url)
    {
        $this->authorize('editar-planos');
        $plan = $this->plan->where('url', $url)->first();

        if (!$plan) {
            $this->messageStatus('warning');
            return redirect()->route('plans.index');
        }

        return view('admin.pages.plans.edit', compact('plan'));
    }

    public function update(StoreUpdatePlan $request, $url)
    {
        $this->authorize('editar-planos');
        $plan = $this->plan->where('url', $url)->first();

        if (!$plan) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $plan->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('plans.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('plans.index');
        }
    }

    public function destroy($url)
    {
        $this->authorize('deletar-planos');
        $plan = $this->plan
            ->with('details')
            ->where('url', $url)
            ->first();

        if (!$plan) {
            $this->messageStatus('warning');
            return redirect()->back();
        }


        if ($plan->details->count() > 0 ) {
            $this->messageStatus('info');
            return redirect()->back();
        }

        try {
            $plan->delete();

            $this->messageStatus('success');
            return redirect()->route('plans.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('plans.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $plans = $this->plan->search($request->filter);

        return view('admin.pages.plans.index', compact('plans', 'filters'));
    }
}
