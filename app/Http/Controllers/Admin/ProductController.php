<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateCategory;
use App\Http\Requests\StoreUpdateProduct;
use App\Models\Product;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    use MessageTrait;

    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index()
    {
        $this->authorize('visualizar-produtos');
        $products = $this->product->latest()->paginate();

        return view('admin.pages.products.index', compact('products'));
    }

    public function create()
    {
        $this->authorize('cadastrar-produtos');
        return view('admin.pages.products.create');
    }

    public function store(StoreUpdateProduct $request)
    {
        $this->authorize('cadastrar-produtos');
        try {
            $data = $request->all();
            $tenant = auth()->user()->tenant;

            if ($request->hasFile('image') && $request->image->isValid()) {
                $data['image'] = $request->image->store("tenants/{$tenant->uuid}/products");
            }

            $this->product->create($data);
            $this->messageStatus('success');

            return redirect()->route('products.index');

        } catch (\Exception $exception) {
            dd($exception);
            $this->messageStatus('error');
            return redirect()->route('products.index');
        }
    }

    public function show($id)
    {
        $this->authorize('visualizar-produtos');
        $product = $this->product->where('id', $id)->first();

        if (!$product) {
            $this->messageStatus('warning');
            return redirect()->route('products.index');
        }
        return view('admin.pages.products.show', compact('product'));
    }

    public function edit($id)
    {
        $this->authorize('editar-produtos');
        $product = $this->product->where('id', $id)->first();

        if (!$product) {
            $this->messageStatus('warning');
            return redirect()->route('products.index');
        }

        return view('admin.pages.products.edit', compact('product'));
    }

    public function update(StoreUpdateProduct $request, $id)
    {
        $this->authorize('editar-produtos');
        $product = $this->product->find($id);

        if (!$product) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $data = $request->all();
            $tenant = auth()->user()->tenant;

            if ($request->hasFile('image') && $request->image->isValid()) {

                if (Storage::exists($product->image)) {
                    Storage::delete($product->image);
                }

                $data['image'] = $request->image->store("tenants/{$tenant->uuid}/products");
            }
            $product->update($data);

            $this->messageStatus('success');
            return redirect()->route('products.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('products.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-produtos');
        $product = $this->product->find($id);

        if (!$product) {
            $this->messageStatus('warning');
            return redirect()->back();
        }

        try {
            if (Storage::exists($product->image)) {
                Storage::delete($product->image);
            }

            $product->delete();

            $this->messageStatus('success');
            return redirect()->route('products.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('products.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $products = $this->product->search($request->filter);

        return view('admin.pages.products.index', compact('products', 'filters'));
    }
}
