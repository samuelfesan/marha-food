<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateTable;
use App\Models\Table;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class TableController extends Controller
{
    use MessageTrait;

    private $table;

    public function __construct(Table $table)
    {
        $this->table = $table;
    }

    public function index()
    {
        $this->authorize('visualizar-mesas');
        $tables = $this->table->latest()->paginate();

        return view('admin.pages.tables.index', compact('tables'));
    }

    public function create()
    {
        $this->authorize('cadastrar-mesas');
        return view('admin.pages.tables.create');
    }

    public function store(StoreUpdateTable $request)
    {
        $this->authorize('cadastrar-mesas');
        try {

            $this->table->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('tables.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('tables.index');
        }
    }

    public function show($id)
    {
        $this->authorize('visualizar-mesas');
        $table = $this->table->where('id', $id)->first();

        if (!$table) {
            $this->messageStatus('warning');
            return redirect()->route('tables.index');
        }
        return view('admin.pages.tables.show', compact('table'));
    }

    public function edit($id)
    {
        $this->authorize('editar-mesas');
        $table = $this->table->where('id', $id)->first();

        if (!$table) {
            $this->messageStatus('warning');
            return redirect()->route('tables.index');
        }

        return view('admin.pages.tables.edit', compact('table'));
    }

    public function update(StoreUpdateTable $request, $id)
    {
        $this->authorize('editar-mesas');
        $table = $this->table->find($id);

        if (!$table) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $table->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('tables.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('tables.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-mesas');
        $table = $this->table->find($id);

        if (!$table) {
            $this->messageStatus('warning');
            return redirect()->back();
        }

        try {
            $table->delete();

            $this->messageStatus('success');
            return redirect()->route('tables.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('tables.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $tables = $this->table->search($request->filter);

        return view('admin.pages.tables.index', compact('tables', 'filters'));
    }
}
