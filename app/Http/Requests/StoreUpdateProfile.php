<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:3|max:255|unique:profiles,name',
            'description' => 'nullable|min:3|max:255',
        ];

        if ($this->method() == 'PUT') {
            $rules['name'] = "required|min:3|max:255|unique:profiles,name,{$this->profile},id";
        }

        return $rules;
    }
}
