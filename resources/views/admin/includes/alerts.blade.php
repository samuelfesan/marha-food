@if ($erros->any())
    <div class="alert alert-warning">
        @foreach($erros->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
