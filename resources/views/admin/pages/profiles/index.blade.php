@extends('adminlte::page')


@section('title', __('Perfis'))

@section('content_header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('profiles.index') }}">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('profiles.index') }}">{{ __('Perfis') }}</a></li>
    </ol>
    <h1>{{ __('Perfis') }}
        @can('cadastrar-perfis')
            <a href="{{ route('profiles.create') }}" class="btn btn-success">{{ __('Novo') }}
                <i class="fas fa-plus-square"></i>
            </a>
        @endcan
    </h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            {!! Form::open(['route' => 'permissions.search', 'class' => 'form form-inline']) !!}
                <input type="text" name="filter" class="form-control" placeholder="{{ __('Procurar') }}..." value="{{ $filters['filter'] ?? '' }}">
                <button type="submit" class="btn btn-primary">
                    {{ __('Filtrar') }}
                </button>
            {!! Form::close() !!}
        </div>
        <div class="card-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>{{ __('Nome') }}</th>
                    <th>{{ __('Descrição') }}</th>
                    <th>{{ __('Ações') }}</th>
                </tr>
                </thead>
                <tbody>

                @forelse($profiles as $profile)
                    <tr>
                        <td>{{ $profile->name }}</td>
                        <td>{{ $profile->description }}</td>
                        <td>
                            <a href="{{ route('profiles.show', $profile->id) }}" title="{{ __("Visualizar Detalhes") }}"><i class='fas fa-eye'></i></a>
                            @can('editar-perfis')
                                <a href="{{ route("profiles.edit", $profile->id) }}" title="{{ __("Editar") }}"><i class="fas fa-edit"></i></a>
                            @endcan
                            <a href="{{ route("profiles.permissions", $profile->id) }}" title="{{ __("Visualizar permissões") }}">
                                <i class="fas fa-lock"></i>
                            </a>

                            <a href="{{ route("profiles.plans", $profile->id) }}" title="{{ __("Visualizar planos") }}">
                                <i class="fas fa-book"></i></a>

                            @can('deletar-perfis')
                                {{ Form::open(['route' => ['profiles.destroy', $profile->id], 'method' => 'DELETE', 'style' => 'display:inline']) }}

                                <a href="#javascript"
                                   class='delete_btn demo3'
                                   data-toggle='btn_del' title="{{ __("Excluir") }}">
                                    <i class='fas fa-trash-alt' style="color:#df4740"></i>
                                </a>
                                {{ Form::close() }}
                            @endcan
                        </td>
                    </tr>
                @empty

                @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if(isset($filters))
                {{ $profiles->appends($filters)->links() }}
            @else
                {{ $profiles->links() }}
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $('[data-toggle=btn_del]').click(function () {
            var form = $(this).closest('form');

            swal({
                title: "Você tem certeza?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Sim, pode deletar!"]
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Deletado!", "", "success");
                        setTimeout(function() {
                            form.submit() }, 1000);
                    } else {
                        swal("Cancelado!", "", "success");
                    }
                });
        });
    </script>

@endsection

