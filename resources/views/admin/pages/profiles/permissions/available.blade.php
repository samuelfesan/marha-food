@extends('adminlte::page')


@section('title', __('Permissões disponíveis'))

@section('content_header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('profiles.index') }}">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('profiles.index') }}">{{ __('Perfis') }}</a></li>
    </ol>
    <h1>{{ __('Permissões disponíveis perfil') }} {{ $profile->name ?? '' }}</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            {!! Form::open(['route' => ['profiles.permissions.available', $profile->id], 'class' => 'form form-inline', 'method' => 'post']) !!}
                <input type="text" name="filter" class="form-control" placeholder="{{ __('Procurar') }}..." value="{{ $filters['filter'] ?? '' }}">
                <button type="submit" class="btn btn-primary">
                    {{ __('Filtrar') }}
                </button>
            {!! Form::close() !!}
        </div>
        <div class="card-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th width="50px">#</th>
                    <th>{{ __('Nome') }}</th>
                </tr>
                </thead>
                <tbody>
                    {!! Form::open(['route' => ['profiles.permissions.attach', $profile->id], 'class' => 'form form-inline', 'method' => 'post']) !!}

                        @forelse($permissions as $permission)
                            <tr>
                                <td>
                                    <input type="checkbox" name="permissions[]" value="{{ $permission->id }}">
                                </td>
                                <td>{{ $permission->name }}</td>
                            </tr>
                        @empty

                        @endforelse
                        <tr>
                            <td colspan="500">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Salvar') }}
                                </button>
                            </td>

                        </tr>
                    {!! Form::close() !!}
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if(isset($filters))
                {{ $permissions->appends($filters)->links() }}
            @else
                {{ $permissions->links() }}
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $('[data-toggle=btn_del]').click(function () {
            var form = $(this).closest('form');

            swal({
                title: "Você tem certeza?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Sim, pode deletar!"]
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Deletado!", "", "success");
                        setTimeout(function() {
                            form.submit() }, 1000);
                    } else {
                        swal("Cancelado!", "", "success");
                    }
                });
        });
    </script>

@endsection

