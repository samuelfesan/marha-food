@extends('adminlte::page')

@section('title', __('Detalhes do Produto'))

@section('content_header')
    <h1>{{ __('Detalhes do Produto') }} <strong>{{ $product->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $product->name }}</li>
                <li><strong>{{ __('Imagem') }}: </strong>
                    <img src="{{ url("storage/{$product->image}") }}" alt="{{ $product->name }}"
                         style="max-width:100px">
                </li>
                <li><strong>{{ __('Url') }}: </strong>{{ $product->url }}</li>
                <li><strong>{{ __('Preço R$') }}: </strong>{{ number_format($product->price, 2, ',', '.') }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $product->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
