@extends('adminlte::page')

@section('title', _('Editar Produto'))

@section('content_header')
    <h1>{{ __('Editar Produto') }} <strong>{{ $plan->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($product, ['route' => ['products.update', 'product' => $product->id], 'class' => 'form', 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.pages.products.partials.form')
    {!! Form::close() !!}

@endsection
