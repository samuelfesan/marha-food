@extends('adminlte::page')

@section('title', __('Cadastrar Produto'))

@section('content_header')
    <h1>{{ __('Cadastrar Produto') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'products.store', 'class' => 'form', 'files' => true]) !!}
        @include('admin.pages.products.partials.form')
    {!! Form::close() !!}

@endsection
