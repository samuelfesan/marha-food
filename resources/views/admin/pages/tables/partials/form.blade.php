
<div class="row">
    <div class="col-lg-12">
        <div class="card shadow-lg border-0 rounded-lg ">

            <div class="card-body">
                <form>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-group has-feedback {{ $errors->has('identify') ? 'has-error' : '' }}">
                                {!! Form::label('identify', __('Mesa*')) !!}
                                {!! Form::text('identify', null, ['class' => 'form-control py-4', 'id'  => 'identify', 'placeholder' => __('Informe o identificador')]) !!}
                                {!! $errors->first('identify', '<span class="help-block"><strong>:message</strong></span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group  has-feedback {{ $errors->has('description') ? 'has-error' : '' }}">
                        {!! Form::label('description', __('Descrição')) !!}
                        {!! Form::textarea('description', null, ['class' => 'form-control py-4', 'id'  => 'description', 'placeholder' => __('Informe a descrição')]) !!}
                        {!! $errors->first('description', '<span class="help-block"><strong>:message</strong></span>') !!}
                    </div>
                </form>

                <div class="col-sm-6">

                    <button type="submit" class="btn btn-primary">
                        {{ __('Salvar') }}
                    </button>

                    <a class="btn btn-white" href="{{ URL::previous() }}">
                        {{ __('Cancelar') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


