@extends('adminlte::page')

@section('title', __('Editar Mesa'))

@section('content_header')
    <h1>{{ __('Editar Mesa') }} <strong>{{ $plan->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($table, ['route' => ['tables.update', 'table' => $table->id], 'class' => 'form', 'method' => 'PUT']) !!}
        @include('admin.pages.tables.partials.form')
    {!! Form::close() !!}

@endsection
