@extends('adminlte::page')


@section('title', __('Mesas'))

@section('content_header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('tables.index') }}">{{ __('Mesas') }}</a></li>
    </ol>
    <h1>{{ __('Mesas') }}
        @can('cadastrar-mesas')
            <a href="{{ route('tables.create') }}" class="btn btn-success">{{ __('Novo') }}
                <i class="fas fa-plus-square"></i>
            </a>
        @endcan
    </h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            {!! Form::open(['route' => 'tables.search', 'class' => 'form form-inline']) !!}
                <input type="text" name="filter" class="form-control" placeholder="Procurar..." value="{{ $filters['filter'] ?? '' }}">
                <button type="submit" class="btn btn-primary">
                    {{ __('Filtrar') }}
                </button>
            {!! Form::close() !!}
        </div>
        <div class="card-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>{{ __('Nome') }}</th>
                    <th>{{ __('Descrição') }}</th>
                    <th>{{ __('Ações') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tables as $table)
                    <tr>
                        <td>{{ $table->identify }}</td>
                        <td>{{ $table->description }}</td>
                        <td>
                            <a href="{{ route('tables.show', $table->id) }}" title="{{ __("Visualizar Detalhes") }}"><i class='fas fa-eye'></i></a>
                            @can('editar-mesas')
                                <a href="{{ route("tables.edit", $table->id) }}" title="{{ __("Editar") }}"><i class="fas fa-edit"></i></a>
                            @endcan

                            @can('deletar-mesas')
                                {{ Form::open(['route' => ['tables.destroy', $table->id], 'method' => 'DELETE', 'style' => 'display:inline']) }}

                                <a href="#javascript"
                                   class='delete_btn demo3'
                                   data-toggle='btn_del'>
                                    <i class='fas fa-trash-alt' style="color:#df4740"></i>
                                </a>
                                {{ Form::close() }}
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if(isset($filters))
                {{ $tables->appends($filters)->links() }}
            @else
                {{ $tables->links() }}
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $('[data-toggle=btn_del]').click(function () {
            var form = $(this).closest('form');

            swal({
                title: "Você tem certeza?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Sim, pode deletar!"]
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Deletado!", "", "success");
                        setTimeout(function() {
                            form.submit() }, 1000);
                    } else {
                        swal("Cancelado!", "", "success");
                    }
                });
        });
    </script>

@endsection

