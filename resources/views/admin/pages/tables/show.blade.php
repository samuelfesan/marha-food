@extends('adminlte::page')

@section('title', __('Detalhes da Mesa'))

@section('content_header')
    <h1>{{ __('Detalhes da Mesa') }} <strong>{{ $table->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Mesa') }}: </strong>{{ $table->identify }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $table->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
