@extends('adminlte::page')

@section('title', __('Cadastrar Mesa'))

@section('content_header')
    <h1>{{ __('Cadastrar Mesa') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'tables.store', 'class' => 'form']) !!}
        @include('admin.pages.tables.partials.form')
    {!! Form::close() !!}

@endsection
