@extends('adminlte::page')

@section('title', __('Detalhes do Plano'))

@section('content_header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('plans.index') }}">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('plans.index') }}">{{ __('Planos') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('plans.show', $plan->url) }}">{{ $plan->name }}</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('details.plan.index', $plan->url) }}">{{ __('Detalhes do Plano') }}</a></li>
    </ol>
    <h1>{{ __('Detalhes do Plano') }} {{ $plan->name }}
        @can('cadastrar-planos')
            <a href="{{ route('details.plan.create', $plan->url) }}" class="btn btn-success">{{ __('Novo') }}
                <i class="fas fa-plus-square"></i>
            </a>
        @endcan
    </h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>{{ __('Nome') }}</th>
                    <th>{{ __('Ações') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($details as $detail)
                    <tr>
                        <td>{{ $detail->name }}</td>
                        <td>
                            <a href="{{ route('details.plan.show', [$plan->url, $detail->id]) }}" title="{{ __('Visualizar Detalhes') }}"><i class='fas fa-eye'></i></a>

                            @can('editar-planos')
                                <a href="{{ route("details.plan.edit", [$plan->url, $detail->id]) }}" title="{{ __('Editar') }}"><i class="fas fa-edit"></i></a>
                            @endcan

                            @can('deletar-planos')
                                {{ Form::open(['route' => ['details.plan.destroy', 'url' => $plan->url, 'idDetail' => $detail->id], 'method' => 'DELETE', 'style' => 'display:inline']) }}

                                <a href="#javascript"
                                   class='delete_btn demo3'
                                   data-toggle='btn_del'>
                                    <i class='fas fa-trash-alt' style="color:#df4740"></i>
                                </a>
                                {{ Form::close() }}
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $('[data-toggle=btn_del]').click(function () {
            var form = $(this).closest('form');

            swal({
                title: "Você tem certeza?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Sim, pode deletar!"]
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Deletado!", "", "success");
                        setTimeout(function() {
                            form.submit() }, 1000);
                    } else {
                        swal("Cancelado!", "", "success");
                    }
                });
        });
    </script>
@endsection

