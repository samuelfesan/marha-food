@extends('adminlte::page')

@section('title', __('Cadastrar Detalhe Plano'))

@section('content_header')
    <h1>{{ __('Cadastrar Detalhe do Plano') }} <strong>{{ $plan->name }}</strong></h1>
@stop

@section('content')

    {!! Form::open(['route' => ['details.plan.store', 'url' => $plan->url], 'class' => 'form']) !!}
        @include('admin.pages.plans.details.partials.form')
    {!! Form::close() !!}

@endsection
