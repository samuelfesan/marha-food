@extends('adminlte::page')

@section('title', __('Editar Detalhe do Plano'))

@section('content_header')
    <h1>{{ __('Editar Detalhe do Plano') }} <strong>{{ $detail->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($detail, ['route' => ['details.plan.update',
            'url' => $plan->url, 'idDetail' => $detail->id], 'class' => 'form', 'method' => 'PUT']) !!}
        @include('admin.pages.plans.details.partials.form')
    {!! Form::close() !!}

@endsection
