@extends('adminlte::page')

@section('title', __('Detalhes do Planos'))

@section('content_header')
    <h1>{{ __('Detalhes do Plano') }} <strong>{{ $plan->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $plan->name }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
