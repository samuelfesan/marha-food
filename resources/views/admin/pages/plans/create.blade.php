@extends('adminlte::page')

@section('title', __('Cadastrar Plano'))

@section('content_header')
    <h1>{{ __('Cadastrar Plano') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'plans.store', 'class' => 'form']) !!}
        @include('admin.pages.plans.partials.form')
    {!! Form::close() !!}

@endsection
