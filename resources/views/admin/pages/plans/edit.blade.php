@extends('adminlte::page')

@section('title', __('Editar Plano'))

@section('content_header')
    <h1>{{ __('Editar Plano') }} <strong>{{ $plan->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($plan, ['route' => ['plans.update', 'url' => $plan->url], 'class' => 'form', 'method' => 'PUT']) !!}
        @include('admin.pages.plans.partials.form')
    {!! Form::close() !!}

@endsection
