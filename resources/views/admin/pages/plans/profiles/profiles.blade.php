@extends('adminlte::page')


@section('title', __('Perfis do Plano'))

@section('content_header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('plans.index') }}">{{ __('Perfis') }}</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('plans.profiles', $plan->id) }}">{{ __('Planos') }}</a></li>
    </ol>
    <h1>{{ __('Perfis do Plano') }} {{ $plan->name ?? '' }}
        <a href="{{ route('plans.profiles.available', $plan->id) }}"
           class="btn btn-success">{{ __('Novo') }}
            <i class="fas fa-plus-square"></i>
        </a>
    </h1>
@stop

@section('content')
    <div class="card">

        <div class="card-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>{{ __('Nome') }}</th>
                    <th>{{ __('Descrição') }}</th>
                    <th>{{ __('Ações') }}</th>
                </tr>
                </thead>
                <tbody>

                @forelse($profiles as $profile)
                    <tr>
                        <td>{{ $profile->name }}</td>
                        <td>{{ $profile->description }}</td>
                        <td>
                            {{ Form::open(['route' => ['plans.profiles.detach', $plan->id, $profile->id],
                            'method' => 'DELETE', 'style' => 'display:inline']) }}

                            <a href="#javascript"
                               class='delete_btn demo3'
                               data-toggle='btn_del'>
                                <i class='fas fa-trash-alt' style="color:#df4740"></i>
                            </a>
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty

                @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if(isset($filters))
                {{ $profiles->appends($filters)->links() }}
            @else
                {{ $profiles->links() }}
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $('[data-toggle=btn_del]').click(function () {
            var form = $(this).closest('form');

            swal({
                title: "Você tem certeza?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Sim, pode deletar!"]
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Deletado!", "", "success");
                        setTimeout(function() {
                            form.submit() }, 1000);
                    } else {
                        swal("Cancelado!", "", "success");
                    }
                });
        });
    </script>

@endsection

