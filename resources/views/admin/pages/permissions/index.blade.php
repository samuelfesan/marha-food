@extends('adminlte::page')


@section('title', __('Permissões'))

@section('content_header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('permissions.index') }}">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('permissions.index') }}">{{ __('Permissões') }}</a></li>
    </ol>
    <h1>{{ __('Permissões') }}
        @can('cadastrar-permissoes')
            <a href="{{ route('permissions.create') }}" class="btn btn-success">{{ __('Novo') }}
                <i class="fas fa-plus-square"></i>
            </a>
        @endcan
    </h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            {!! Form::open(['route' => 'permissions.search', 'class' => 'form form-inline']) !!}
                <input type="text" name="filter" class="form-control" placeholder="{{ __('Procurar') }}..." value="{{ $filters['filter'] ?? '' }}">
                <button type="submit" class="btn btn-primary">
                    {{ __('Filtrar') }}
                </button>
            {!! Form::close() !!}
        </div>
        <div class="card-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>{{ __('Nome') }}</th>
                    <th>{{ __('Descrição') }}</th>
                    <th>{{ __('Ações') }}</th>
                </tr>
                </thead>
                <tbody>

                @forelse($permissions as $permission)
                    <tr>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $permission->description }}</td>
                        <td>
                            <a href="{{ route('permissions.show', $permission->id) }}" title="{{ __('Visualizar Detalhes') }}"><i class='fas fa-eye'></i></a>
                            @can('editar-permissoes')
                                <a href="{{ route("permissions.edit", $permission->id) }}" title="{{ __('Editar') }}"><i class="fas fa-edit"></i></a>
                            @endcan
                            <a href="{{ route("permissions.profiles", $permission->id) }}" title="{{ __('Visualizar Perfis') }}"><i class="fas fa-book"></i></a>
                            @can('deletar-permissoes')
                                {{ Form::open(['route' => ['permissions.destroy', $permission->id], 'method' => 'DELETE', 'style' => 'display:inline']) }}

                                <a href="#javascript"
                                   class='delete_btn demo3'
                                   data-toggle='btn_del'>
                                    <i class='fas fa-trash-alt' style="color:#df4740"></i>
                                </a>
                                {{ Form::close() }}
                            @endcan
                        </td>
                    </tr>
                @empty

                @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if(isset($filters))
                {{ $permissions->appends($filters)->links() }}
            @else
                {{ $permissions->links() }}
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $('[data-toggle=btn_del]').click(function () {
            var form = $(this).closest('form');

            swal({
                title: "Você tem certeza?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Sim, pode deletar!"]
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Deletado!", "", "success");
                        setTimeout(function() {
                            form.submit() }, 1000);
                    } else {
                        swal("Cancelado!", "", "success");
                    }
                });
        });
    </script>

@endsection

