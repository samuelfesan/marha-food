@extends('adminlte::page')

@section('title', __('Cadastrar Usuário'))

@section('content_header')
    <h1>{{ __('Cadastrar Usuário') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'users.store', 'class' => 'form']) !!}
     @include('admin.pages.users.partials.form')
    {!! Form::close() !!}

@endsection
