@extends('adminlte::page')

@section('title', __('Detalhes do Usuário'))

@section('content_header')
    <h1>{{ __('Detalhes do Usuário') }} <strong>{{ $user->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $user->name }}</li>
                <li><strong>{{ __('E-mail') }}: </strong>{{ $user->email }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
