<?php

return [

    /*
    /Return messages of application
    */

    'success' => [
        'message' => 'Registro salvo com sucesso',
        'title' => 'Sucesso',
        'code' => 201,
    ],

    'info' => [
        'message' => 'Desculpe, esse registro possui detalhes, por isso não pode ser excluído',
        'title' => 'Atenção',
        'code' => 200
    ],

    'warning' => [
        'message' => 'Registro não encontrado',
        'title' => 'Atenção',
        'code' => 404
    ],

    'error' => [
        'message' => 'Ocorreu um erro na aplicação, contate o administrador',
        'title' => 'Erro',
        'code' => 500
    ],

];
